package database.myresume.myresume.me;

import database.myresume.myresume.me.generated.GeneratedMeManager;

/**
 * The main interface for the manager of every {@link
 * database.myresume.myresume.me.Me} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface MeManager extends GeneratedMeManager {}