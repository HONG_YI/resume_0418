package database.myresume.myresume.me;

import database.myresume.myresume.me.generated.GeneratedMeSqlAdapter;

/**
 * The SqlAdapter for every {@link database.myresume.myresume.me.Me} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class MeSqlAdapter extends GeneratedMeSqlAdapter {}