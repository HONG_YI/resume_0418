package database.myresume.myresume.me;

import database.myresume.myresume.me.generated.GeneratedMeImpl;

/**
 * The default implementation of the {@link
 * database.myresume.myresume.me.Me}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MeImpl 
extends GeneratedMeImpl 
implements Me {}