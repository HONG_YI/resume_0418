package database.myresume.myresume.me.generated;

import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.config.identifier.ColumnIdentifier;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.field.IntField;
import com.speedment.runtime.field.StringField;
import com.speedment.runtime.typemapper.TypeMapper;
import database.myresume.myresume.me.Me;

/**
 * The generated base for the {@link database.myresume.myresume.me.Me}-interface
 * representing entities of the {@code me}-table in the database.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public interface GeneratedMe {
    
    /**
     * This Field corresponds to the {@link Me} field that can be obtained using
     * the {@link Me#getId()} method.
     */
    IntField<Me, Integer> ID = IntField.create(
        Identifier.ID,
        Me::getId,
        Me::setId,
        TypeMapper.primitive(),
        true
    );
    /**
     * This Field corresponds to the {@link Me} field that can be obtained using
     * the {@link Me#getAboutme()} method.
     */
    StringField<Me, String> ABOUTME = StringField.create(
        Identifier.ABOUTME,
        Me::getAboutme,
        Me::setAboutme,
        TypeMapper.identity(),
        false
    );
    
    /**
     * Returns the id of this Me. The id field corresponds to the database
     * column Myresume.myresume.me.id.
     * 
     * @return the id of this Me
     */
    int getId();
    
    /**
     * Returns the aboutme of this Me. The aboutme field corresponds to the
     * database column Myresume.myresume.me.aboutme.
     * 
     * @return the aboutme of this Me
     */
    String getAboutme();
    
    /**
     * Sets the id of this Me. The id field corresponds to the database column
     * Myresume.myresume.me.id.
     * 
     * @param id to set of this Me
     * @return   this Me instance
     */
    Me setId(int id);
    
    /**
     * Sets the aboutme of this Me. The aboutme field corresponds to the
     * database column Myresume.myresume.me.aboutme.
     * 
     * @param aboutme to set of this Me
     * @return        this Me instance
     */
    Me setAboutme(String aboutme);
    
    enum Identifier implements ColumnIdentifier<Me> {
        
        ID      ("id"),
        ABOUTME ("aboutme");
        
        private final String columnId;
        private final TableIdentifier<Me> tableIdentifier;
        
        Identifier(String columnId) {
            this.columnId        = columnId;
            this.tableIdentifier = TableIdentifier.of(    getDbmsId(), 
                getSchemaId(), 
                getTableId());
        }
        
        @Override
        public String getDbmsId() {
            return "Myresume";
        }
        
        @Override
        public String getSchemaId() {
            return "myresume";
        }
        
        @Override
        public String getTableId() {
            return "me";
        }
        
        @Override
        public String getColumnId() {
            return this.columnId;
        }
        
        @Override
        public TableIdentifier<Me> asTableIdentifier() {
            return this.tableIdentifier;
        }
    }
}