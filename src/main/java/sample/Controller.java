package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.MyresumeApplication;
import database.MyresumeApplicationBuilder;
import database.myresume.myresume.me.Me;
import database.myresume.myresume.me.MeImpl;
import database.myresume.myresume.me.MeManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.util.Optional;

public class Controller {
    public HTMLEditor txtAboutMe;
    public Button btnOK;
    public WebView web;
    public Button doGetAboutMe;

    public void onGetReadMe(ActionEvent actionEvent) {
        System.out.println(txtAboutMe.getHtmlText());


        MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder().withUsername("root")
                .withPassword("123456").withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM).build();

        MeManager meManager = myresumeApplication.getOrThrow(MeManager.class);
        meManager.persist(new MeImpl().setAboutme(txtAboutMe.getHtmlText()));





        WebEngine webEngine =web.getEngine();
        webEngine.loadContent(txtAboutMe.getHtmlText(),"text/html");
    }

    public void OnGetAboutMe(ActionEvent actionEvent) {
        MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder().withUsername("root")
                .withPassword("123456").withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM).build();
        MeManager meManager = myresumeApplication.getOrThrow(MeManager.class);
        Optional<Me> meOptional = meManager.stream().filter(Me.ID.equal(4)).findAny();

        WebEngine webEngine =web.getEngine();
        if(meOptional.isPresent()){
            webEngine.loadContent(meOptional.get().getAboutme(),"text/html");
        }
        else {
            webEngine.load("http://www.google.com");
        }

    }
}
